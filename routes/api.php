<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resources([
    'items' => 'App\ItemController',
]);

Route::resources([
    'products' => 'Api\ProductController'
]);

Route::resources([
    'orders' => 'Api\OrderController'
]);

Route::get('order-session',
    'Api\OrderController@getOrderSession');


Route::resources([
    'clients' => 'Api\ClientController'
]);
