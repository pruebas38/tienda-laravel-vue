require('./bootstrap');

import Vue from 'vue'
import Paid from './paid/app'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faPlusSquare, faTrash } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faPlusSquare, faTrash)

Vue.component('font-awesome-icon', FontAwesomeIcon)


const paid = new Vue({
    el: '#paid',
    components: { Paid }
});
