<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Product::orderBy('created_at', 'DESC')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newProduct = new Product();
        $newProduct->name = $request->product['name'];
        $newProduct->image = $request->product['image'];
        $newProduct->description = $request->product['description'];
        $newProduct->price = $request->product['price'];
        $newProduct->save();

        return $newProduct;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $existingProduct = Product::find($id);
        if($existingProduct) {
            return $existingProduct;
        }

        return "Product not found";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $existingProduct = Product::find($id);

        if($existingProduct) {
            $existingProduct->name = $request->product['name'];
            $existingProduct->image = $request->product['image'];
            $existingProduct->description = $request->product['description'];
            $existingProduct->price = $request->product['price'];
            $existingProduct->save();
            return $existingProduct;
        }

        return "Product not found";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $existingProduct = Product::find($id);

        if($existingProduct) {
            $existingProduct->delete();
            return "Product successfully deleted";
        }

        return "Product not found";
    }
}
