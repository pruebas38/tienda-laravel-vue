<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Order::orderBy('created_at', 'DESC')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newOrder = new Order();
        $newOrder->customer_name = $request->order['customer_name'];
        $newOrder->customer_email = $request->order['customer_email'];
        $newOrder->customer_mobile = $request->order['customer_mobile'];
        $newOrder->status = 'CREATED';
        $newOrder->reference = md5(Carbon::now());
        $newOrder->product_id = $request->order['product_id'];
        $newOrder->save();

        $OrderSession = $this->getOrderSession();

        $result = [
            'newOrder' => $newOrder,
            'OrderSession' => $OrderSession
        ];

        return $result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $existingOrder = Order::find($id);
        if($existingOrder) {
            return $existingOrder;
        }

        return "Order not found";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $existingOrder = Order::find($id);

        if($existingOrder) {
            $existingOrder->status = $request->order['status'];
            $existingOrder->save();
            return $existingOrder;
        }

        return "Order not found";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $existingOrder = Order::find($id);

        if($existingOrder) {
            $existingOrder->delete();
            return "Client successfully deleted";
        }

        return "Client not found";
    }

    public function getOrderSession()
    {
        $seed = Carbon::now()->toIso8601String();
        $expiration = Carbon::now()->addMinutes(15)->toIso8601String();
        $secretKey = '024h1IlD';

        if (function_exists('random_bytes')) {
            $nonce = bin2hex(random_bytes(16));
        } elseif (function_exists('openssl_random_pseudo_bytes')) {
            $nonce = bin2hex(openssl_random_pseudo_bytes(16));
        } else {
            $nonce = mt_rand();
        }

        $nonceBase64 = base64_encode($nonce);

        $tranKey = base64_encode(sha1($nonce . $seed . $secretKey, true));

        $result = [
            'login' => '6dd490faf9cb87a9862245da41170ff2',
            'nonce' => $nonceBase64,
            'tranKey' => $tranKey,
            'seed' => $seed,
            'expiration' => $expiration
        ];

        return $result;
    }
}
