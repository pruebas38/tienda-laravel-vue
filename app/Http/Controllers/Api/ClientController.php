<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Client::orderBy('created_at', 'DESC')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newClient = new Client();
        $newClient->name = $request->client['name'];
        $newClient->address = $request->client['address'];
        $newClient->mobile = $request->client['mobile'];
        $newClient->status = 'ACTIVE';
        $newClient->save();

        return $newClient;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $existingClient = Client::find($id);
        if($existingClient) {
            return $existingClient;
        }

        return "Client not found";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $existingClient = Client::find($id);

        if($existingClient) {
            $existingClient->name = $request->client['name'];
            $existingClient->address = $request->client['address'];
            $existingClient->mobile = $request->client['mobile'];
            $existingClient->status = $request->client['status'];
            $existingClient->save();
            return $existingClient;
        }

        return "Client not found";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $existingClient = Client::find($id);

        if($existingClient) {
            $existingClient->delete();
            return "Client successfully deleted";
        }

        return "Client not found";
    }
}
